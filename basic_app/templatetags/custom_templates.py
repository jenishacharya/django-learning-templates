from django import template

register = template.Library()

def cut(value, arg):
    """
    this cuts out all values of 'arg' from the string
    """
    return value.replace(arg, '')

def sq_add(value, arg):
    """
    squares the value and adds the given argument
    """
    return (value  * value) + arg

# to register template filter: ('name_through_which_it_can_be_called', function_name)
register.filter('cut', cut)
register.filter('sqa', sq_add)